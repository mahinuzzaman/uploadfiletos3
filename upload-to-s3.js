var express = require('express');
var config = require('dotenv');
var bodyParser = require('body-parser');
const serverless = require('serverless-http');
const UploadRoutes = require('./api/server/routes/UploadRoutes');
const cors = require('cors');



config.config();

const app = express();

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(cors())

const port = process.env.PORT || 8800;


app.use('/api/v1/upload', UploadRoutes);


app.get('*', (req, res) => res.status(200).send({
    message: 'Welcome to upload file API.',
}));


app.listen(port, () => {
    console.log(`Server is running on PORT ${port}`);
});

//export default app;
//module.exports.handler = serverless(app);