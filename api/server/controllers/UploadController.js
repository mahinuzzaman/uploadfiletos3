var AWS = require('aws-sdk');
const util = require('../utils/Utils');
var s3 = new AWS.S3();
const resizeHandler = require('../Services/resizeHandler')

exports.TransformImage = async(req, res) => {
    console.log(req.body);





    let extList = ["png", "jpeg"]

    if (extList.indexOf(req.body.Ext) > -1) {
        try {

            const imagePath = await resizeHandler._process(req)
                //const URL = `http://${process.env.BUCKET}.s3-website.${process.env.REGION}.amazonaws.com`


            if (imagePath != null) {
                util.setSuccess(200, 'Image uploaded and thumb added!', {
                    ImagePath: req.body.S3path,
                    ThumbImagePath: req.body.ThumbS3path,

                });
                util.send(res);
            } else {
                util.setError(400, 'Failed to upload');
                util.send(res);
            }
        } catch (error) {
            util.setError(400, error);
            util.send(res);
        }
    } else {
        util.setSuccess(200, 'File uploaded successfully!', {
            FilePath: req.body.S3path

        });
        util.send(res);

    }

}

// let lists3files=async()=>{
//     var params = { 
//         Bucket: 'squad-bench-bucket',
//         Delimiter: '/',
//         Prefix: 'test-image-upload2/'
//         }

//         s3.listObjects(params, function (err, data) {
//         if(err)throw err;
//         console.log(data);
//         res.status(200).send(data);
//         });
// }