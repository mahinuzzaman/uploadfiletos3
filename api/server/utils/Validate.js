const Validator = require('validatorjs');


// const validator = (body, rules, customMessages, callback) => {
//     const validation = new Validator(body, rules, customMessages);
//     validation.passes(() => callback(null, true));
//     validation.fails(() => callback(validation.errors, false));
// };

let validator = (body, rules, customMessages,callback) => {
	const validation = new Validator(body, rules, customMessages);

	if (validation.passes()) {
		return {status : true, data : null};
    }
    else return  {status : false, data : validation.errors};
}

module.exports = {validator};


