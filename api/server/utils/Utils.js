let statusCode_r = null;
let type_r = '';
let data_r = null;
let message_r = null;


exports.setSuccess = async (statusCode, message, data) => {
	statusCode_r = statusCode;
	message_r = message;
	data_r = data;
	type_r = 'success';
};

exports.setError = async (statusCode, message) => {
	// return {
	// 	"statusCode": statusCode,
	// 	"message": message,
	// 	"type": "error"
	// }

	statusCode_r = statusCode;
	message_r = message;
	type_r = 'error';
};

exports.send = async (res) => {
	// const result = {
	// 	status: type,
	// 	message: message,
	// 	data: data,
	// };

	if (type_r === 'success') {
		return res.status(statusCode_r).json({
			status: statusCode_r,
			type: type_r,
			data: data_r,
		});
	}
	return res.status(statusCode_r).json({
		status: statusCode_r,
		type: type_r,
		message: message_r,
	});
};


