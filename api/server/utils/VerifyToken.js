const jwt = require('jsonwebtoken');
require('dotenv').config();

let verifyToken = (req, res, next) => {
    let token = req.headers['x-access-token'];

    if (!token) {
        return res.status(403).send({
            auth: false,
            message: 'No token provided.'
        });
    }

    jwt.verify(token, process.env.SECRET_KEY, (err, decoded) => {
        if (err) {
            return res.status(401).send({
                auth: false,
                message: 'Fail to Authentication. Error -> ' + err
            });
        }
        //console.log(decoded.id);
        req.user_id = decoded.id;
        req.body.created_by = decoded.id;
        next();
    });
}



const authJwt = {};
authJwt.verifyToken = verifyToken;

module.exports = authJwt;