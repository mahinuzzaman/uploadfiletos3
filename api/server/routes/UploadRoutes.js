const express = require("express");
//const multipart = require('connect-multiparty')
//const multipartMiddleware = multipart()
const UploadController = require("../controllers/UploadController");
const authJwt = require("../utils/VerifyToken");

const router = express.Router();

var aws = require("aws-sdk");
var multer = require("multer");
var multerS3 = require("multer-s3");

var app = express();
var s3 = new aws.S3({
    /* ... */
});

var upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: process.env.BUCKET,
        metadata: function(req, file, cb) {

            cb(null, { fieldName: file.fieldname });
            //console.log(req);


            //console.log(req.body);
        },
        key: function(req, file, cb) {
            cb(
                null,
                (req.body.Folder ? req.body.Folder + "/" : "") +
                req.body.Filename +
                (req.body.Ext ? "." + (req.body.Ext == "jpg" ? "jpeg" : req.body.Ext) : "")
            );
        },
    }),
});


var checkFields = async(req, res, next) => {
    console.log(req.body);

    if (!req.body.Size) req.body.Size = "250x250";

    var nameext = req.body.Filename.split('.').pop();

    if (!req.body.Ext) {

        req.body.Ext = nameext;
    }
    if (req.body.Ext == "jpg") {

        req.body.Ext = "jpeg";
    }

    req.body.ThumbS3path =
        "thumb/" +
        (req.body.Folder ? req.body.Folder + "/" : "") +
        req.body.Filename +
        (req.body.Ext ? "." + req.body.Ext : "");


    req.body.S3path =
        (req.body.Folder ? req.body.Folder + "/" : "") +
        req.body.Filename +
        (req.body.Ext ? "." + req.body.Ext : "");


    var nameext = req.body.Filename.split('.').pop();

    if (!req.body.Ext) {

        req.body.Ext = nameext;
    }
    if (req.body.Ext == "jpg") {
        console.log(req.body.Ext);
        req.body.Ext = "jpeg";
    }
    next();
}


router.post("/uploadfile", [authJwt.verifyToken], upload.array("FileToUpload", 1), checkFields, UploadController.TransformImage);

module.exports = router;