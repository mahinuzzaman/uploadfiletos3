var AWS = require('aws-sdk');
var stream = require('stream');

AWS.config.region = 'ap-southeast-1'
const S3 = new AWS.S3()
exports.isKeyExists = async({ Bucket, Key }) => {
    const params = {
        Bucket: Bucket,
        Key: Key
    }
    try {
        return await S3.headObject(params).promise()

    } catch (err) {
        return false;
    }

}
exports.readStream = async({ Bucket, Key }) => {
    return await S3.getObject({ Bucket, Key }).createReadStream()
}

exports.writeStream = async({ Bucket, Key, Ext }) => {
    const passThrough = new stream.PassThrough()
    return await {
        writeStream: passThrough,
        uploaded: S3.upload({
            ContentType: 'image/' + Ext,
            Body: passThrough,
            Bucket,
            Key
        }).promise()
    }
}