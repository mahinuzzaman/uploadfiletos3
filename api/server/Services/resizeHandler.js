const s3Handler = require("./s3Handler");

//Core image processing package
const sharp = require("sharp");

exports._process = async(req) => {
    //console.log("4444444444");
    const size = req.body.Size;
    const image = req.body.S3path;
    return await resize(req, size);
};

let resize = async(req, size) => {
    //console.log(sharp.format)
    try {
        const sizeArray = size.split("x");
        const width = parseInt(sizeArray[0]);
        const height = parseInt(sizeArray[1]);

        const Key = req.body.S3path;
        const newKey = req.body.ThumbS3path;

        const Bucket = process.env.BUCKET;
        let isexistres = await s3Handler.isKeyExists({ Bucket, Key });
        if (isexistres == false) {
            return null;
        }

        const streamResize = await sharp().resize(width, height).toFormat(req.body.Ext);
        const readStream = await s3Handler.readStream({ Bucket, Key });

        const { writeStream, uploaded } = await s3Handler.writeStream({
            Bucket,
            Key: newKey,
            Ext: req.body.Ext,
        });

        //console.log(req.body)
        //data streaming
        readStream.pipe(streamResize).pipe(writeStream);

        //console.log("2222");
        await uploaded;
        //console.log("33333");
        return newKey;
    } catch (error) {
        throw new Error(error);
    }
};